package ast

type Application struct {
	Function Node
	Args     []Node
}

func NewApplication(function Node, args ...Node) *Application {
	return &Application{
		Function: function,
		Args:     args,
	}
}

func (a Application) String() string {
	return a.Function.Name() + "(" + Stringify(a.Args) + ")"
}

func (a Application) Name() string {
	return a.Function.Name()
}
