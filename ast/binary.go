package ast

type Binary struct {
	Left, Right Node
	Op          string
}

func NewBinary(left, right Node, op string) *Binary {
	return &Binary{
		Left:  left,
		Right: right,
		Op:    op,
	}
}

func (b Binary) String() string {
	return b.Left.String() + b.Op + b.Right.String()
}

func (b Binary) Name() string {
	return b.Op
}
