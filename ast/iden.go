package ast

type Identifier struct {
	value string
}

func NewIdentifier(name string) *Identifier {
	return &Identifier{value: name}
}

func (i Identifier) String() string {
	return i.value
}

func (i Identifier) Name() string {
	return i.value
}
