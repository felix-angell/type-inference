package ast

type Let struct {
	name Node
	body Node
}

func NewLet(name, body Node) *Let {
	return &Let{
		name: name,
		body: body,
	}
}

func (l Let) Name() string {
	return "uhhh"
}
