package ast

type Node interface {
	Name() string
	String() string
}

func Stringify(nodes []Node) string {
	var result string
	for i, n := range nodes {
		result += n.String()
		if i != len(nodes)-1 {
			result += ", "
		}
	}
	return result
}
