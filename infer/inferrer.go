package infer

import (
	"github.com/felixangell/type-inference/ast"
	"github.com/felixangell/type-inference/types"
	"strconv"
)

type TypeInferrer struct{}

func NewTypeInferrer() *TypeInferrer {
	return &TypeInferrer{}
}

func occursIn(v types.Type, types []types.Type) bool {
	for _, typ := range types {
		if occursInType(v, typ) {
			return true
		}
	}
	return false
}

func occursInType(v types.Type, b types.Type) bool {
	pb := prune(b)
	if v == pb {
		return true
	}

	switch pb := pb.(type) {
	case *types.TypeOperator:
		return occursIn(v, pb.GetTypes())
	}
	return false
}

func unify(a, b types.Type) {
	pa := prune(a)
	pb := prune(b)

	switch pa := pa.(type) {
	case *types.TypeVariable:
		if pa != pb {
			// recursion cehck ehre
			pa.Instance = pb
		}
	case *types.TypeOperator:
		switch pb := pb.(type) {
		case *types.TypeVariable:
			unify(pb, pa)
		case *types.TypeOperator:
			if (pa.Name() != pb.Name()) || (len(pa.GetTypes()) != len(pb.GetTypes())) {
				panic("Type mismatch: '" + pa.String() + "' with '" + pb.String() + "'")
			}
			for idx, a := range pa.GetTypes() {
				unify(a, pb.GetTypes()[idx])
			}
		default:
			panic("Not unified '" + pb.String() + "'")
		}
	}
}

func prune(typ types.Type) types.Type {
	switch typ := typ.(type) {
	case *types.TypeVariable:
		if typ.Instance != nil {
			return typ.Instance
		}
	}
	return typ
}

func fresh(typ types.Type) types.Type {
	var real_fresh func(typ types.Type) types.Type
	real_fresh = func(typ types.Type) types.Type {
		pruned := prune(typ)
		switch pruned := pruned.(type) {
		case *types.TypeVariable:
			return pruned
		case *types.TypeOperator:
			newTypes := make([]types.Type, len(pruned.GetTypes()))
			for i, typ := range pruned.GetTypes() {
				newTypes[i] = real_fresh(typ)
			}
			return types.NewTypeOperator(pruned.Name(), newTypes...)
		case *types.Function:
			newTypes := make([]types.Type, len(pruned.GetTypes()))
			for i, typ := range pruned.GetTypes() {
				newTypes[i] = real_fresh(typ)
			}
			return types.NewFunction(real_fresh(pruned.Ret), newTypes)
		default:
			panic("Bad type '" + pruned.String() + "'")
		}
	}

	return real_fresh(typ)
}

func (t *TypeInferrer) getType(name string, env map[string]types.Type) types.Type {
	if typ, ok := env[name]; ok {
		return fresh(typ)
	} else if _, err := strconv.ParseInt(name, 10, 64); err == nil {
		return types.Integer
	} else if _, err := strconv.ParseFloat(name, 64); err == nil {
		return types.Double
	}
	switch name[0] {
	case '"':
		if name[len(name)-1] == '"' {
			return types.String
		}
		break
	case '\'':
		if name[len(name)-1] == '\'' {
			return types.Rune
		}
		break
	}

	panic("Undefined symbol '" + name + "'")
}

func (t *TypeInferrer) Analyze(node ast.Node, env map[string]types.Type) types.Type {
	switch node := node.(type) {
	case *ast.Identifier:
		return t.getType(node.Name(), env)
	case *ast.Application:
		funcType := t.Analyze(node.Function, env)
		switch funcType := funcType.(type) {
		case *types.Function:
			if len(funcType.Args) != len(node.Args) {
				panic("Type mismatch!")
			}
			for idx, arg := range node.Args {
				unify(t.Analyze(arg, env), funcType.Args[idx])
			}
			return funcType.Ret
		}
		panic("well this is awkward")
	case *ast.Binary:
		left := t.Analyze(node.Left, env)
		right := t.Analyze(node.Right, env)
		unify(left, right)
		return left
	}

	panic("Unhandled node '" + node.String() + "'")
}
