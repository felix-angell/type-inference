package main

import (
	"github.com/felixangell/type-inference/infer"
	"github.com/felixangell/type-inference/ast"
	"github.com/felixangell/type-inference/types"
	"fmt"
)

func test(infer *infer.TypeInferrer, n ast.Node, env map[string]types.Type) {
	typ := infer.Analyze(n, env)
	fmt.Println(n, ":", typ)
}

func main() {
	environment := map[string]types.Type{
		"true": types.Bool,
		"false": types.Bool,
		"foo": types.NewFunction(types.Integer, []types.Type{types.Integer}),
	}
	inferrer := infer.NewTypeInferrer()
	try := func(n ast.Node) {
		fmt.Print("> ")
		test(inferrer, n, environment)
	};

	try(ast.NewIdentifier("true"))
	try(ast.NewIdentifier("333.2"))
	try(ast.NewIdentifier("66"))

	try(ast.NewIdentifier("\"hello, world\""))
	try(ast.NewIdentifier("'h'"))

	// function call
	try(ast.NewApplication(ast.NewIdentifier("foo"), ast.NewIdentifier("5")))

	// binary operators
	try(ast.NewBinary(ast.NewIdentifier("5.3"), ast.NewIdentifier("3.141"), "+"))
	try(ast.NewBinary(ast.NewIdentifier("3.123"), ast.NewIdentifier("3.141"), "+"))
}
