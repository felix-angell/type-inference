package types

type Function struct {
	name string
	Args []Type
	Ret  Type
}

func (f Function) Name() string {
	return f.name
}

func (f *Function) GetTypes() []Type {
	return f.Args
}

func (f *Function) String() string {
	return Stringify(f.Args) + " -> " + f.Ret.String()
}

func NewFunction(to Type, from []Type) *Function {
	return &Function{
		name: "->",
		Args: from,
		Ret:  to,
	}
}
