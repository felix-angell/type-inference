package types

type TypeOperator struct {
	name  string
	types []Type
}

func (o *TypeOperator) String() string {
	typeCount := len(o.types)
	switch typeCount {
	case 0: // no types, print as `T`
		return o.name
	case 2: // 2 types, print as `T name T`
		return o.types[0].String() + o.name + o.types[1].String()
	}
	// `name T, ... Tn, Tn-1`
	return Stringify(o.types)
}

func (o *TypeOperator) Name() string {
	return o.name
}

func (o *TypeOperator) GetTypes() []Type {
	return o.types
}

func NewTypeOperator(name string, types ...Type) *TypeOperator {
	return &TypeOperator{
		name:  name,
		types: types,
	}
}
