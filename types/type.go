package types

var (
	Bool    *TypeOperator = NewTypeOperator("bool")
	Integer               = NewTypeOperator("int")
	Double                = NewTypeOperator("double")
	String                = NewTypeOperator("string")
	Rune                  = NewTypeOperator("rune")
)

type Type interface {
	String() string
	Name() string
	GetTypes() []Type
}

func Stringify(t []Type) string {
	var result string
	for i, typ := range t {
		result += typ.String()
		if i != len(t)-1 {
			result += ", "
		}
	}
	return result
}
