package types

var (
	next_id   int  = -1
	next_name rune = 'a'
)

type TypeVariable struct {
	name     string
	id       int
	Instance Type
	types    []Type
}

func (v *TypeVariable) Name() string {
	// lazily allocate a name for the type variable
	if v.name == "" {
		v.name = string(next_name)

		next_ascii := int(next_name)
		next_ascii++
		next_name = rune(next_ascii)
	}
	return v.name
}

func (v *TypeVariable) GetTypes() []Type {
	return v.types
}

func (v *TypeVariable) String() string {
	if v.Instance != nil {
		return v.Instance.String()
	}
	return v.Name()
}

func NewTypeVariable() *TypeVariable {
	next_id++
	return &TypeVariable{
		id: next_id,
	}
}
